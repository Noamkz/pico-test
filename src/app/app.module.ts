import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HeaderComponent } from "./header/header.component";

import { AppComponent } from "./app.component";
import { ServersComponent } from "./servers/servers.component";
import { SettingComponentComponent } from "./setting-component/setting-component.component";
import { Routes, RouterModule } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";
import { StorageServiceModule } from "angular-webstorage-service";

const appRoutes: Routes = [
  //192.168.1.6:4200/setting    local host
  { path: "", component: ServersComponent },
  { path: ":timer/:numOfCard", component: ServersComponent },
  { path: "setting", component: SettingComponentComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ServersComponent,
    HeaderComponent,
    SettingComponentComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    StorageServiceModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
