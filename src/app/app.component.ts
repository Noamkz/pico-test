import { Component, OnInit, Output, Inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { LOCAL_STORAGE, WebStorageService } from "angular-webstorage-service";

// מסך פתיחה בדיקה אם משתמש חדש אם כן יבוא נתונים מהשרת אם לא יבוא נתנוים מהזיכרון
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  loading = true;
  @Output() dataDb = [];
  massge: string = "Loading...";

  constructor(
    public http: HttpClient,
    @Inject(LOCAL_STORAGE) private storage: WebStorageService
  ) {}

  onActivate(componentReference) {
    componentReference.passingData(this.dataDb);
  }

  async ngOnInit() {
    if (this.storage.get("user_id") === null) {
      this.http
        .get("https://dev-bot.pico.buzz/memory")
        .pipe(
          map(responseData => {
            const postsArray = [];
            for (const key in responseData) {
              if (responseData.hasOwnProperty(key)) {
                postsArray.push({ ...responseData[key], id: key });
              }
            }

            return postsArray;
          })
        )
        .toPromise()
        .then(data => {
          this.loading = false;
          this.dataDb = data[0].images;
          this.storage.set("user_id", data[0].user_id);
          this.storage.set("images", data[0].images);
        })
        .catch(err => {
          this.massge = "Opps, Error: " + err.status;
        });
    } else {
      this.dataDb = await this.storage.get("images");
      this.loading = false;
    }
  }
}
