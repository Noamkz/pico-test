import { Card } from "./card.model";

// אובייקט הגדרות לוח משחק
export class BordSetting {
  public firstLogin: boolean;
  public backCardImg: string =
    "../../assets/images/c159b4738dae9c9d8d6417228024de8d.jpg";
  public memoryCards: Card[];
  public timer: number;
  public result: boolean;
  public timerStart: boolean;
  public numOfCard: number;
  public score: number = 0;
  public height: number = window.innerHeight * 0.25 - 35;
  public cards = [];
  public cardsList: Card[] = [];

  constructor(
    firstLogin: boolean = false,
    memoryCards: Card[] = [],
    timer: number = 20,
    result: boolean = null,
    timerStart: boolean = false,
    numOfCard: number = 8,
    cards = []
  ) {
    this.firstLogin = firstLogin;
    this.memoryCards = memoryCards;
    this.timer = timer;
    this.result = result;
    this.timerStart = timerStart;
    this.numOfCard = numOfCard;
    this.cards = cards;
  }
}
