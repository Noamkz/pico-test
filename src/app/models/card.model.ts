// מחלקה קלף
export class Card {
  constructor(
    public index: number,
    public id: string,
    public imageUrl: string,
    public pair_id: string,
    public flipe: boolean = false,
    public showCard: string = "../../assets/images/c159b4738dae9c9d8d6417228024de8d.jpg",
    public back: boolean = false
  ) {}
}
