import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
//מסך הגדרות משחק
@Component({
  selector: "app-setting-component",
  templateUrl: "./setting-component.component.html",
  styleUrls: ["./setting-component.component.css"]
})
export class SettingComponentComponent implements OnInit {
  public timer: number = 20;
  public numOfCard: number = 8;

  constructor(private router: Router) {}

  ngOnInit() {}

  save = () => {
    this.router.navigate([`/`, this.timer, this.numOfCard]);
  };

  passingData(data) {}

  changeNumOfCard = opt => {
    this.numOfCard = opt.target.value;
  };

  timeChange = opt => {
    this.timer = opt.target.value;
  };
}
