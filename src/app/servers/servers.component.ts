import { Component, OnInit, Input, Inject } from "@angular/core";
import { Card } from "../models/card.model";
import { BordSetting } from "../models/bordSetting.model";
import { ActivatedRoute } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { LOCAL_STORAGE, WebStorageService } from "angular-webstorage-service";

// המסך הראשי - לוח המשחק
@Component({
  selector: "app-servers",
  templateUrl: "./servers.component.html",
  styleUrls: ["./servers.component.css"]
})
export class ServersComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    public http: HttpClient,
    @Inject(LOCAL_STORAGE) private storage: WebStorageService
  ) {}

  BordSet: BordSetting = new BordSetting();
  @Input() dataDb: [];
  startTime: number;

  passingData(data) {
    this.dataDb = data;
  }

  ngOnInit() {
    // מהתחל משחק ומציג את מסך הפתיחה
    this.restrt();
    this.BordSet.firstLogin = true;
  }
  restrt = () => {
    // מאפס את המשחק לחדש
    this.BordSet = new BordSetting();
    this.BordSet.cards = this.dataDb;
    if (this.route.snapshot.params["timer"] !== undefined) {
      this.BordSet.timer = this.route.snapshot.params["timer"];
      this.BordSet.numOfCard = this.route.snapshot.params["numOfCard"];
    }
    this.BordSet.cardsList.map(card => {
      card.flipe = !card.flipe;
    });
    this.startTime = this.BordSet.timer;
    this.BordSet.cardsList = [];
    let playedCards = this.BordSet.cards.filter((card, i) => {
      return i < this.BordSet.numOfCard;
    });
    playedCards
      .sort(function() {
        return 0.5 - Math.random();
      })
      .map((card, i) => {
        this.BordSet.cardsList.push(
          new Card(i, card.id, card.image, card.pair_id)
        );
      });
  };

  bagin = () => {
    this.BordSet.firstLogin = false;
  };

  clickCard = (index: number) => {
    var timerInterval;
    if (!this.BordSet.cardsList[index].flipe) {
      //--------------------------
      // התחלת השעון
      if (!this.BordSet.timerStart) {
        this.BordSet.timerStart = true;
        timerInterval = setInterval(() => {
          if (this.BordSet.score === this.BordSet.cardsList.length / 2) {
            //בדיקה  האם התוצאה שווה לניצחון
            clearInterval(timerInterval);
            this.BordSet.result = true;
            // עדכן שרת בתוצאה
            this.http
              .post("https://dev-bot.pico.buzz/memory", {
                user_id: this.storage.get("user_id"),
                time: this.startTime - this.BordSet.timer,
                matches: this.BordSet.score
              })
              .subscribe(
                res => {
                  console.log(res);
                },
                err => {
                  console.log(err);
                }
              );
          } else if (this.BordSet.timer === 0) {
            this.BordSet.cardsList.map(card => {
              card.flipe = !card.flipe;
            });
            // כאשר לשחקן נגמר הזמן
            clearInterval(timerInterval);
            this.BordSet.result = false;
            // עדכן שרת בתוצאה
            this.http
              .post("https://dev-bot.pico.buzz/memory", {
                user_id: this.storage.get("user_id"),
                time: this.startTime,
                matches: this.BordSet.score
              })
              .subscribe(
                res => {
                  console.log(res);
                },
                err => {
                  console.log(err);
                }
              );
          } else {
            this.BordSet.timer = this.BordSet.timer - 1; //הקטנת הזמן בשניה
          }
        }, 1000);
      }
      //-------------------------

      if (this.BordSet.memoryCards.length < 2 && this.BordSet.timer > 0) {
        //הפיכת הקלף והכנסה למחסנית
        this.BordSet.cardsList[index].showCard = this.BordSet.cardsList[
          index
        ].imageUrl;
        this.BordSet.cardsList[index].back = true;
        if (this.BordSet.memoryCards.length === 0)
          this.BordSet.memoryCards.push(this.BordSet.cardsList[index]);
        else if (
          this.BordSet.memoryCards.length === 1 &&
          this.BordSet.memoryCards[0].index !== index
        ) {
          this.BordSet.memoryCards.push(this.BordSet.cardsList[index]);
        }

        if (this.BordSet.memoryCards.length === 2) {
          //האם שני קלפים הפוכים במחסנית?
          if (
            this.BordSet.memoryCards[0].id ===
            this.BordSet.memoryCards[1].pair_id
          ) {
            //האם הקפלפים זהיים?
            this.BordSet.score += 1;
            this.BordSet.cardsList[
              this.BordSet.memoryCards[0].index
            ].flipe = true;
            this.BordSet.cardsList[
              this.BordSet.memoryCards[1].index
            ].flipe = true;
            this.BordSet.memoryCards.pop();
            this.BordSet.memoryCards.pop();
          } else {
            setTimeout(() => {
              // הפיכה חזרה לגב והוצאה מהחסנית
              this.BordSet.cardsList[0].back = false;
              this.BordSet.cardsList[1].back = false;
              this.BordSet.cardsList[
                this.BordSet.memoryCards[0].index
              ].showCard = this.BordSet.backCardImg;
              this.BordSet.cardsList[
                this.BordSet.memoryCards[1].index
              ].showCard = this.BordSet.backCardImg;
              this.BordSet.memoryCards.pop();
              this.BordSet.memoryCards.pop();
            }, 1000); // כמה זמן לוקח לשני הקלפים החשופים להיתפך
          }
        }
      }
    }
    //----------------------------------
  };
}
